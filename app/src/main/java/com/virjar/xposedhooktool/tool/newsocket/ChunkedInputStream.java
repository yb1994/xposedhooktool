package com.virjar.xposedhooktool.tool.newsocket;

import android.support.annotation.NonNull;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by virjar on 2018/4/27.<br>
 * decode stream for chunked inputStream
 */
public class ChunkedInputStream extends InputStream {
    private InputStream delegate;

    ChunkedInputStream(InputStream delegate) {
        this.delegate = delegate;
    }

    private static final int CHUNK_LEN = 1;
    private static final int CHUNK_DATA = 2;
    private static final int CHUNK_CRLF = 3;
    private static final int CHUNK_INVALID = Integer.MAX_VALUE;

    private static final int BUFFER_SIZE = 2048;


    private int state;

    /**
     * The chunk size
     */
    private int chunkSize;

    /**
     * The current position within the current chunk
     */
    private int pos;

    /**
     * True if we've reached the end of stream
     */
    private boolean eof = false;

    /**
     * True if this stream is closed
     */
    private boolean closed = false;


    @Override
    public int available() throws IOException {
        final int len = delegate.available();
        return Math.min(len, this.chunkSize - this.pos);
    }

    /**
     * <p> Returns all the data in a chunked stream in coalesced form. A chunk
     * is followed by a CRLF. The method returns -1 as soon as a chunksize of 0
     * is detected.</p>
     * <p>
     * <p> Trailer headers are read automatically at the end of the stream and
     * can be obtained with the getResponseFooters() method.</p>
     *
     * @return -1 of the end of the stream has been reached or the next data
     * byte
     * @throws IOException in case of an I/O error
     */
    @Override
    public int read() throws IOException {
        if (this.closed) {
            throw new IOException("Attempted read from closed stream.");
        }
        if (this.eof) {
            return -1;
        }
        if (state != CHUNK_DATA) {
            nextChunk();
            if (this.eof) {
                return -1;
            }
        }
        final int b = delegate.read();
        if (b != -1) {
            pos++;
            if (pos >= chunkSize) {
                state = CHUNK_CRLF;
            }
        }
        return b;
    }

    /**
     * Read some bytes from the stream.
     *
     * @param b   The byte array that will hold the contents from the stream.
     * @param off The offset into the byte array at which bytes will start to be
     *            placed.
     * @param len the maximum number of bytes that can be returned.
     * @return The number of bytes returned or -1 if the end of stream has been
     * reached.
     * @throws IOException in case of an I/O error
     */
    @Override
    public int read(@NonNull final byte[] b, final int off, final int len) throws IOException {

        if (closed) {
            throw new IOException("Attempted read from closed stream.");
        }

        if (eof) {
            return -1;
        }
        if (state != CHUNK_DATA) {
            nextChunk();
            if (eof) {
                return -1;
            }
        }
        final int bytesRead = delegate.read(b, off, Math.min(len, chunkSize - pos));
        if (bytesRead != -1) {
            pos += bytesRead;
            //只可能等于，不可能大于，等于证明当前chunk读取到最后了，但是可能有下一个chunk
            if (pos >= chunkSize) {
                state = CHUNK_CRLF;
            }
            return bytesRead;
        } else {
            eof = true;
            throw new RuntimeException("Truncated chunk "
                    + "( expected size: " + chunkSize
                    + "; actual size: " + pos + ")");
        }
    }

    /**
     * Read some bytes from the stream.
     *
     * @param b The byte array that will hold the contents from the stream.
     * @return The number of bytes returned or -1 if the end of stream has been
     * reached.
     * @throws IOException in case of an I/O error
     */
    @Override
    public int read(@NonNull final byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    /**
     * Read the next chunk.
     *
     * @throws IOException in case of an I/O error
     */
    private void nextChunk() throws IOException {
        if (state == CHUNK_INVALID) {
            throw new IllegalStateException("Corrupt data stream");
        }
        try {
            chunkSize = getChunkSize();
            if (chunkSize < 0) {
                throw new IllegalStateException("Negative chunk size");
            }
            state = CHUNK_DATA;
            pos = 0;
            if (chunkSize == 0) {
                eof = true;
            }
        } catch (IllegalStateException ex) {
            state = CHUNK_INVALID;
            throw ex;
        }
    }

    /**
     * Expects the stream to start with a chunksize in hex with optional
     * comments after a semicolon. The line must end with a CRLF: "a3; some
     * comment\r\n" Positions the stream at the start of the next line.
     */
    private int getChunkSize() throws IOException {
        final int st = this.state;
        switch (st) {
            case CHUNK_CRLF:

                String s;
                try {
                    s = HttpStreamUtil.readLine(delegate, Integer.MAX_VALUE);
                } catch (RuntimeException e) {
                    throw new IllegalStateException(
                            "CRLF expected at end of chunk", e);
                }
                if (!StringUtils.isBlank(s)) {
                    throw new IllegalStateException(
                            "Unexpected content at the end of chunk");
                }
                state = CHUNK_LEN;
                //$FALL-THROUGH$
            case CHUNK_LEN:
                String buffer;
                try {
                    buffer = HttpStreamUtil.readLine(delegate, Integer.MAX_VALUE);
                } catch (RuntimeException e) {
                    throw new IllegalStateException("Premature end of chunk coded message body: " +
                            "closing chunk expected", e);
                }

                int separator = buffer.indexOf(';');
                if (separator < 0) {
                    separator = buffer.length();
                }
                try {
                    return Integer.parseInt(StringUtils.trim(buffer.substring(0, separator)), 16);
                } catch (final NumberFormatException e) {
                    throw new IllegalStateException("Bad chunk header");
                }
            default:
                throw new IllegalStateException("Inconsistent codec state");
        }
    }

    /**
     * Upon close, this reads the remainder of the chunked message,
     * leaving the underlying socket at a position to start reading the
     * next response without scanning.
     *
     * @throws IOException in case of an I/O error
     */
    @Override
    public void close() throws IOException {
        if (!closed) {
            try {
                if (!eof && state != CHUNK_INVALID) {
                    // read and discard the remainder of the message
                    final byte buff[] = new byte[BUFFER_SIZE];
                    while (read(buff) >= 0) {
                        //
                    }
                }
            } finally {
                eof = true;
                closed = true;
            }
        }
    }
}
